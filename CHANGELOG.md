# AmberCrtish Color Theme - Change Log

## [0.3.2]

- tweak widget/notification borders and backgrounds

## [0.3.1]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.3.0]

- brighten contrasting panels
- dim borders
- retain v0.2.5 for those that prefer earlier style

## [0.2.5]

- update readme and screenshot

## [0.2.4]

- fix source control graph badge colors: dim scmGraph.historyItemRefColor

## [0.2.3]

- fix titlebar border, FG/BG in custom mode

## [0.2.2]

- fix manifest and pub WF

## [0.2.1]

- dimmed button BG brightened slightly for better contrast in extension sidebar

## [0.2.0]

- dimmer buttons BG so as not to visually compete with Sidebar Git project names
- fix repo links in manifest
- retain v0.1.2 for those who prefer style

## [0.1.2]

- make badge BG transparent

## [0.1.1]

- swap button.FG/BG

## [0.1.0]

- alter terminal ansi color set for more expression
- retain prior version for those who prefer that style

## [0.0.3]

- fix terminal cursor BG

## [0.0.2]

- soften "editor.lineHighlightBorder" to "#FFBF0080"

## [0.0.1]

- Initial release
